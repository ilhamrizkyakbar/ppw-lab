# -*- coding: utf-8 -*-
from django.test import TestCase
from django.test import Client
from django.urls import resolve
class Lab8UnitTest(TestCase):
	def test_lab_8_url_is_exist(self):
		response = Client().get('/lab-8/')
		self.assertEqual(response.status_code, 200)

# Create your tests here.
